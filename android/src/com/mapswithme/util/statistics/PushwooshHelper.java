package com.mapswithme.util.statistics;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;

import com.mapswithme.maps.MwmApplication;
import com.mapswithme.util.log.Logger;
import com.mapswithme.util.log.LoggerFactory;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public final class PushwooshHelper
{
  private static final Logger LOGGER = LoggerFactory.INSTANCE.getLogger(LoggerFactory.Type.MISC);
  private static final PushwooshHelper sInstance = new PushwooshHelper();

  private WeakReference<Context> mContext;

  private final Object mSyncObject = new Object();
  private AsyncTask<Void, Void, Void> mTask;
  private List<Map<String, Object>> mTagsQueue = new LinkedList<>();

  private PushwooshHelper() {}

  public static PushwooshHelper get() { return sInstance; }

  public void setContext(Context context)
  {
    synchronized (mSyncObject)
    {
      mContext = new WeakReference<>(context);
    }
  }

  public void synchronize()
  {
    sendTags(null);
  }

  public void sendTag(String tag, Object value)
  {
    Map<String, Object> tags = new HashMap<>();
    tags.put(tag, value);
    sendTags(tags);
  }

  private void sendTags(Map<String, Object> tags)
  {
  }

  private boolean canSendTags()
  {
    return mContext != null && mTask == null;
  }

  public static native void nativeProcessFirstLaunch();
  public static native void nativeSendEditorAddObjectTag();
  public static native void nativeSendEditorEditObjectTag();
}
